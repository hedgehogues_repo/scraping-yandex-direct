from time import sleep


class CaptchaProcessor:
    """
        Обработчик событий, связанных с капчей
    """

    """
        Конструктор

        captcha_repeat_times : None, uint
            None задаёт бесконечное количество попыток ввода капчи, число -- ограничивает количество попыток ввода
        captcha_timeout : uint
            Время ожидания, которое будет отведено на ввод капчи
    """

    def __init__(self, base_driver, captcha_repeat_times=None, captcha_timeout=5000):
        self.__milliseconds = 1000

        self.captcha_repeat_times_ = captcha_repeat_times
        self.captcha_timeout_ = captcha_timeout / self.__milliseconds

        self.driver_ = base_driver

        self.__captcha_button = None
        self.__captcha_image = None

    def __select_button(self, text_buttons):
        for button in self.driver_.find_elements_by_css_selector('button'):
            if button.text in text_buttons:
                self.__captcha_button = button
                return

    def get_captcha_button(self):
        return self.__captcha_button

    def get_captcha_image(self):
        return self.__captcha_image

    def is_captcha(self):
        class_names_captcha_elements = ['captcha', 'b-captcha-form__captcha']
        text_buttons = ['Войти', 'OK']
        for capthca, button in zip(class_names_captcha_elements, text_buttons):
            elements = self.driver_.find_elements_by_class_name(capthca)
            if len(elements) != 0:
                self.__captcha_image = elements[0].get_attribute('src')
                self.__select_button(text_buttons)
                sleep(self.captcha_timeout_)
                return True
        return False
