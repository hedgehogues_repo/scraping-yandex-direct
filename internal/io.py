from internal.sqliteDb import SqliteDb


class ReadWriter:
    def __init__(self):
        self.__table = []

    def close(self):
        pass

    def flush(self):
        self.__table = []

    def read(self, aud0, aud1):
        for item in self.__table:
            if item[0] == aud0 and item[1] == aud1 or item[1] == aud0 and item[0] == aud1:
                return item
        return ()

    def write(self, aud0, aud1, intersection):
        for item in self.__table:
            if item[0] == aud0 and item[1] == aud1 or item[1] == aud0 and item[0] == aud1:
                item[2] = intersection
                return
        self.__table.append((aud0, aud1, intersection))


class ReadWriterSQLite:
    def __init__(self, path, table_name='campaigns'):
        self.table_name = table_name
        self.db = SqliteDb(path)
        self.db.connect()
        self.db.clear_table()

    def flush(self):
        self.db.clear_table()

    def close(self):
        self.db.close()

    def read(self, aud0, aud1):
        item = self.db.get_rec(aud0,aud1)
        return item

    def write(self, aud0, aud1, intersection):
        self.db.update_rec_campaigns(aud0, aud1, intersection)
