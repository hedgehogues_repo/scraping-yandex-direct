import random
from time import sleep
from warnings import warn

from selenium.webdriver.support.wait import WebDriverWait


class Driver:
    """
        Элементарные действия, осуществляемые на странице
    """

    """
        Конструктор

        Parameters
        ----------
        action_timeout:
            Таймаут между действиями на странице
        enter_text_timeout : uint
            Таймаут между вводом отдельных символов при вводе какого-либо текста
        repeat_big_timeout_tics : uint
            Количество действий (тиков), по накоплении которых будет активирован большой таймаут
        random_big_timeout : uint
            Величина большого таймаут
    """

    def __init__(self, base_driver, captcha, action_timeout=300, random_timeout=200, enter_text_timeout=20,
                 random_big_timeout=10000, repeat_big_timeout_tics=100):
        self.__milliseconds = 1000
        self.__tic = 0

        self.captcha_ = captcha
        self.driver_ = base_driver
        self.action_timeout_ = action_timeout / self.__milliseconds
        self.random_timeout_ = random_timeout
        self.enter_text_timeout_ = enter_text_timeout / self.__milliseconds
        self.random_big_timeout_ = random_big_timeout / self.__milliseconds
        self.repeat_big_timeout_tics_ = repeat_big_timeout_tics

        # Hardcode clear cache parameter
        self.waiter = WebDriverWait(self.driver_, 60)

    @staticmethod
    def __get_clear_browsing_button(driver):
        return driver.find_element_by_css_selector('* /deep/ #clearBrowsingDataConfirm')

    def clear_cache(self):
        # navigate to the settings page
        self.driver_.get('chrome://settings/clearBrowserData')

        # wait for the button to appear
        self.waiter.until(self.__get_clear_browsing_button)

        # click the button to clear the cache
        self.__get_clear_browsing_button(self.driver_).click()

        # wait for the button to be gone before returning
        self.waiter.until_not(self.__get_clear_browsing_button)

        self.driver_.delete_all_cookies()

    def close(self):
        self.driver_.quit()

    def big_timeout(self):
        if self.repeat_big_timeout_tics_ == self.__tic:
            sleep(self.action_timeout_ + self.get_rand_time() * self.action_timeout_)

    def take_tic(self):
        self.__tic += 1
        self.big_timeout()

    def get_rand_time(self):
        return random.randint(0, self.random_timeout_) / self.__milliseconds

    def set_text(self, xpath, text):
        login_edit = self.driver_.find_element_by_xpath(xpath)
        for char in text:
            self.send_keys(element=login_edit, char=char)

    def click(self, element):
        step = 0
        # Captcha loop
        while True:
            try:
                element.click()
            except:
                warn('Undefined button')
            sleep(self.action_timeout_ + self.get_rand_time())
            self.take_tic()
            step += 1
            if not ((self.captcha_.captcha_repeat_times_ is None or step < self.captcha_.captcha_repeat_times_) and
                    self.captcha_.is_captcha()):
                return
            element = self.captcha_.get_captcha_button()

    def find_element_by_xpath(self, xpath):
        input_button = self.driver_.find_element_by_xpath(xpath)
        return input_button

    def maximize_window(self):
        self.driver_.maximize_window()
        sleep(self.action_timeout_ + self.get_rand_time())

    def get_url(self, url):
        self.driver_.get(url)

    def get_title(self):
        return self.driver_.title

    def send_keys(self, element, char):
        element.send_keys(char)
        sleep(self.enter_text_timeout_ + self.get_rand_time())
        self.take_tic()

    def find_element_by_text(self, class_name, target_text, element=None):
        elements = []
        steps = 0
        while True:
            element = self.driver_ if element is None else element
            candidates = element.find_elements_by_css_selector('[class="' + class_name + '"]')
            for candidate in candidates:
                if target_text is None or candidate.text in target_text:
                    elements.append(candidate)
            sleep(self.action_timeout_ + self.get_rand_time())
            if len(elements) != 0:
                break
            if steps > self.repeat_find_times:
                assert False, 'There is not correct element'
            steps += 1
        return elements
