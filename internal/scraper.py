import re


class Scraper:
    """
        Класс предназначен для извлечения данных из сервиса Яндекс.Директ. Рассматривается множество аудиторий.
        Созданный экземпляр класса позволяет осуществлять попарное пересечение аудиторий, получать список всех аудиторий
        и пересекать заданную аудиторию со всеми остальными.
    """

    """
        Конструктор. В конструкторе указываются задержки. Все задержки задаются в миллисекундах (0.001 секунды).
        
        При выполнении скрипта производится подсчёт действий, которые были произведены на страницах сервиса Яндекса. 
        Количество действий учитывается. При накоплении их большого количества запускает длинный таймаут. 
        
        Parameters
        ----------
        login : str
            Логин
        pwd : str
            Пароль
        browser_cases : BrowserCases
            Объект, который выполняет примитивные действия, необходимые для навигации по страницам и для скрапинга
        readwriter : ReadWriter
            Объект, производящий чтение и запись 
        base_url : str
            Url по которому произойдет переход при запуске
        repeat_find_times : uint or None
            Столько раз будет искаться элемент на странице. Если None, то ожидание будет вечным. После этого -- эксепшн
        is_debug : bool
            Дебаг-версия (присутствует отладочная печать)
        count_ : int
            Количество уже обработанных объектов
    """
    def __init__(self, login, pwd, custom_driver, readwriter=None, is_debug=False, base_url='https://direct.yandex.ru/',
                 repeat_find_times=100):
        self.driver = custom_driver
        self.__readwriter = readwriter
        self.__menu_class_names = [[], []]
        self.__select_menu = [None, None]

        self.base_url_ = base_url
        self.is_debug_ = is_debug
        self.login_ = login
        self.pwd_ = pwd
        self.repeat_find_times_ = repeat_find_times
        self.count_ = 0

        self.driver.get_url(base_url)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.driver.quit()

    def __select_needs_item(self):
        class_name = 'link__inner'
        target_text = ['Новое условие', '"Новое условие"']
        self.driver.click(self.driver.find_element_by_text(class_name=class_name, target_text=target_text)[0])

        class_name = 'radio-button__radio'
        target_text = ['Выполнены все', '"Выполнены все"']
        self.driver.click(self.driver.find_element_by_text(class_name=class_name, target_text=target_text)[0])

    def __change_needs_item(self):
        class_name = 'select select_on-scroll_close select_layout_fixed select_theme_normal select_size_s i-bem ' \
                     'select_js_inited'
        target_text = ['"Цель Метрики"', 'Цель Метрики']
        self.driver.find_element_by_text(class_name=class_name, target_text=target_text)[0].click()

        class_name = 'select__text'
        target_text = ['"Сегмент Аудиторий"', 'Сегмент Аудиторий']
        self.driver.click(self.driver.find_element_by_text(class_name=class_name, target_text=target_text)[0])

    def __get_visible_audience_name(self, two_elements):
        class_name = 'b-chooser__line b-retargeting-condition-edit-rule__list-popup-content-item b-retargeting-' \
                     'condition-edit-rule__list-popup-content-item_type_audience-segment'
        if two_elements[0].is_displayed():
            audience_name = self.driver.find_element_by_text(
                class_name=class_name, target_text=None, element=two_elements[0]
            )[0].text
            self.driver.click(two_elements[0])
        elif two_elements[1].is_displayed():
            audience_name = self.driver.find_element_by_text(
                class_name=class_name, target_text=None, element=two_elements[1]
            )[0].text
            self.driver.click(two_elements[1])
        else:
            assert False, 'Unvisible element'
        return audience_name

    def __find_enter_item(self):
        return self.driver.find_element_by_text(
            class_name='passport-Button-Text', target_text='Войти'
        )[0]

    def __temp_load_page(self):
        step = 0
        # Sometimes the serve responds with temporary page, therefore you need use this loop
        while True:
            self.driver.click(self.__find_enter_item())
            if self.driver.get_title() != 'Мои кампании' and (
                    self.repeat_find_times_ is None or step < self.repeat_find_times_):
                step += 1
            elif self.driver.get_title() != 'Мои кампании' and step >= self.repeat_find_times_:
                assert self.driver.get_title() == 'Мои кампании'
            else:
                break

    def __go_to_scraping_page(self):
        class_name = 'b-additional-actions__group-link'
        target_text = ['"Ретаргетинг и аудитории"', 'Ретаргетинг и аудитории']
        self.driver.click(self.driver.find_element_by_text(class_name=class_name, target_text=target_text)[0])

        self.__select_needs_item()
        self.__change_needs_item()

        class_name = 'link__inner'
        target_text = ['"Добавить правило"', 'Добавить правило']
        self.driver.click(self.driver.find_element_by_text(class_name=class_name, target_text=target_text)[0])

        self.__change_needs_item()

    def __debug_print_audiences(self, aud_name_0, aud_name_1, intersection):
        if self.is_debug_:
            print(aud_name_0, '###', aud_name_1, '###', intersection)

    def __get_intersection_audience_with_another(self):
        self.driver.click(self.__select_menu[0])
        aud_name_0 = self.__get_visible_audience_name(
            self.driver.find_element_by_text(class_name=self.__menu_class_names[0][0], target_text=None)
        )

        for class_name_j in self.__menu_class_names[1]:

            if class_name_j == self.__menu_class_names[0][0]:
                continue

            if len(self.__menu_class_names[1]) != 1:
                self.driver.click(self.__select_menu[1])
                aud_name_1 = self.__get_visible_audience_name(
                    self.driver.find_element_by_text(class_name=class_name_j, target_text=None)
                )

                class_name = 'b-retargeting-condition-wizard__forecast-count'
                count_intersection = self.driver.find_element_by_text(class_name=class_name, target_text=None)[0]
            # Wating
            while len(count_intersection.text) == 0:
                pass

            intersection = int(re.sub('[а-я ]', '', count_intersection.text))
            self.__readwriter.write(aud_name_0, aud_name_1, intersection)
            self.__debug_print_audiences(aud_name_0.replace('\n', ' '), aud_name_1.replace('\n', ' '), intersection)

    def __get_all_audience_pairs(self):
        self.count_ = 0
        for i, class_name_i in enumerate(self.__menu_class_names[0][:-1]):
            self.driver.click(self.__select_menu[0])
            aud_name_0 = self.__get_visible_audience_name(
                self.driver.find_element_by_text(class_name=class_name_i, target_text=None)
            )

            for class_name_j in self.__menu_class_names[1][i + 1:]:
                if len(self.__menu_class_names[1][i + 1:]) != 1:
                    self.driver.click(self.__select_menu[1])
                    aud_name_1 = self.__get_visible_audience_name(
                        self.driver.find_element_by_text(class_name=class_name_j, target_text=None)
                    )
                    class_name = 'b-retargeting-condition-wizard__forecast-count'
                    count_intersection = self.driver.find_element_by_text(class_name=class_name, target_text=None)[0]
                # Wating
                while len(count_intersection.text) == 0:
                    pass

                self.count_ += 1
                intersection = re.sub('[а-я ]', '', count_intersection.text)
                self.__readwriter.write(aud_name_0, aud_name_1, intersection)
                self.__debug_print_audiences(aud_name_0.replace('\n', ' '), aud_name_1.replace('\n', ' '), intersection)

    def __change_top_bottom_selector(self):
        # Detect top and bottom selector
        class_name = 'b-retargeting-condition-edit-rule__list b-retargeting-condition-edit-rule__list_allow-to-use_yes'
        target_text = ['выберите сегмент', '"выберите сегмент"']
        select_menu = self.driver.find_element_by_text(class_name=class_name, target_text=target_text)
        if select_menu[0].location['y'] > select_menu[1].location['y']:
            self.__select_menu[0] = select_menu[1]
            self.__select_menu[1] = select_menu[0]
        else:
            self.__select_menu[0] = select_menu[0]
            self.__select_menu[1] = select_menu[1]

    def __get_class_names_for_audiences(self, audiences):
        self.driver.click(self.__select_menu[0])
        class_name = 'b-chooser__text'
        for element in self.driver.find_element_by_text(class_name=class_name, target_text=None):
            if element.is_displayed() and (audiences is None or audiences is not None and element.text in audiences):
                self.__menu_class_names[0].append(element.find_element_by_xpath('..').get_attribute('class'))
            elif not element.is_displayed():
                self.__menu_class_names[1].append(element.find_element_by_xpath('..').get_attribute('class'))

        # Move mouse to any place
        class_name = 'b-retargeting-condition-edit-popup__header b-modal-popup-decorator__header'
        self.driver.click(
            self.driver.find_element_by_text(class_name=class_name, target_text=None)[0]
        )

    def __extract_audiences_names(self, audiences):
        self.__change_top_bottom_selector()
        self.__get_class_names_for_audiences(audiences)

    """
        Установить новый логин и пароль
        
        Parameters
        ----------
        login : str
            Новый логин
        pwd : str
            Новый пароль
    """
    def set_new_login(self, login, pwd):
        self.login_ = login
        self.pwd_ = pwd

    """
        Залогиниться

        Parameters
        ----------

        Returns
        -------
    """
    def login(self):
        self.driver.maximize_window()
        self.driver.click(self.driver.find_element_by_xpath('//*[@title="Войти"]'))

        self.driver.set_text(xpath='//*//input[@name="login"]', text=self.login_)
        self.driver.set_text(xpath='//*//input[@name="passwd"]', text=self.pwd_)
        self.__temp_load_page()

    def flush(self):
        self.driver.get_url(self.base_url_)
        # Exit
        self.driver.click(self.driver.find_element_by_text('user__name header2__user-name i-bem', None)[0])
        self.driver.click(self.driver.find_element_by_text('b-menu-vert__item b-menu-vert__item_theme_grey', None)[2])

        self.driver.clear_cache()

        self.__menu_class_names = [[], []]
        self.__select_menu = [None, None]

        self.driver.get_url(self.base_url_)

    """
        Завершить выполнение

        Parameters
        ----------

        Returns
        -------
    """
    def close(self):
        self.__readwriter.close()
        self.driver.close()

    """
        Получить значение пересечения аудиторий

        Parameters
        ----------
        aud0 : str
            аудитория 0
        aud1 : str
            аудитория 1

        Returns
        -------
        tuple[str, str, uint]
            аудитория 0, аудитория 1, пересечение
    """
    def read_intersection(self, aud0, aud1):
        return self.__readwriter.read(aud0, aud1)

    """
        Получить список всех аудиторий
        Действия:
            * Переходим на страницу с аудиториями
            * Вытаскиваем названия

        Parameters
        ----------

        Returns
        -------
        list[str]
            список всех аудиторий
    """
    def get_audience_names(self):
        class_name = 'b-additional-actions__group-link'
        target_text = ['"Ретаргетинг и аудитории"', 'Ретаргетинг и аудитории']
        self.driver.click(self.driver.find_element_by_text(class_name=class_name, target_text=target_text)[0])

        self.__select_needs_item()
        self.__change_needs_item()
        elements = self.driver.find_element_by_text(class_name='b-chooser__text', target_text=None)

        audiences = []
        for element in elements:
            audiences.append(element.text)

        self.driver.get_url(self.base_url_)

        return audiences

    """
        Скрапинг данных по пересечениям. Происходит парсинг Яндекс.Директ и сохранение данныхSqliteDb
        Действия:
            * Переходим на страницу с аудиториями
            * Прощёлкиваем все аудитории попарно и вытаскиваем значение пересечения:
                for i in audience:
                    for j in audience[i+1:]:
                        do smth with intersection(i, j)

        Parameters
        ----------
        audiences : str
            Аудитория, с которой нужно осуществлять пересечение

        Returns
        -------

        Raises
        ------
        ValueError
            Если после ввода логина и пароля, после которой следует нажатие кнопки "Войти" произошло действие отличное 
            от:
                - переход на страницу "Мои кампании"
                - переход на страницу с капчей
            генерируется исключение
    """
    def scrapy(self, audiences=None):
        self.__go_to_scraping_page()
        self.__extract_audiences_names(audiences)
        if audiences is None:
            self.__get_all_audience_pairs()
        else:
            self.__get_intersection_audience_with_another()

        self.driver.get_url(self.base_url_)
