import sqlite3


class SqliteDb:
    """
    Класс SqliteDb обеспечивает работу с таблицей кампаний.
Метод __init__ получает один параметр: путь к файлу базы данных.
В БД должна быть создана таблица campaigns

    Ниже приведён скрипт генерации БД:

PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: campaigns
CREATE TABLE campaigns (
    id    INTEGER PRIMARY KEY
                  NOT NULL
                  UNIQUE,
    rule1 VARCHAR NOT NULL,
    rule2 VARCHAR NOT NULL,
    value INTEGER NOT NULL
);

-- Индекс: campaignPairIdx
CREATE UNIQUE INDEX campaignPairIdx ON campaigns (
    rule1,
    rule2
);

COMMIT TRANSACTION;

    После создания экземпляра класс следует выполнить
  коннект к базе.
    Добавление значений в базу можно выполнять 2-мя
  методами:
    add_rec_campaigns (rule1, rule2, value)
    update_rec_campaigns (rule1, rule2, value)

    Метод add_rec_campaigns добавляет запись в таблицу, если в
  таблице есть запись с ключём rule1-rule2, запись добавлена не
  будет, запись в таблице останется без изменений.

    Метод update_rec_campaigns добавляет запись в таблицу при отсутствии
  в таблице записи с ключём rule1-rule2. Если в таблице есть запись с
  ключём rule1-rule2, то старое значение value будет заменено новым.

    Метод get_all_recs() возвращает список кортежей всех записей в таблице.

    Метод count_recs() возвращает количество записей в таблице.

    Метод get_recs(start, step) возвращает step записей из таблицы с позиции start
    и инициализирует итератор, вызвав next() - получаем следующие step записей.


    """

    def __init__(self, fileDb, table_name='campaigns'):
        self.__con = None
        self.__fileDb = fileDb
        self.lastrowid = -1
        self.__cursor = None
        self.__start = 1
        self.__count = -1
        self.curPos = -1

        self.table_name = table_name

    def __del__(self):
        if self.__con is not None:
            self.__con.close()

    def connect(self):
        self.__con = sqlite3.connect(self.__fileDb)
        with self.__con:
            cur = self.__con.cursor()
            cur.execute('SELECT SQLITE_VERSION()')
        if self.__con is not None:
            return True
        else:
            return False

    def close(self):
        if self.__con is not None:
            self.__con.close()
            self.__con = None
        return True

    def add_rec_campaigns(self, rule1, rule2, value):
        """
        Добавляет запись в таблицу campaigns

        :param rule1: значение поля "rule1"
        :param rule2: значение поля "rule2"
        :param value: значение поля "value"
        :return: значение поля "id" для добавленной записи
        """
        lid = -1
        if self.__con is not None:
            cur = self.__con.cursor()
            cur.execute("INSERT INTO " + self.table_name + " (rule1, rule2, value) VALUES (?,?,?)", (rule1, rule2, value))
            self.__con.commit()
            lid = self.__con.total_changes
            self.lastrowid = cur.lastrowid
        return lid

    def update_rec_campaigns(self, rule1, rule2, value):
        """
        Обновляет запись в таблице campaigns.
          Если ключ rule1-rule2 существует, то будет выполнено
        обновление значение value.
          Если ключ rule1-rule2 отсутствует, то будет добавлена запись.

        :param rule1: значение поля "rule1"
        :param rule2: значение поля "rule2"
        :param value: значение поля "value"
        :return: значение поля "id" для добавленной записи
        """
        lid = -1
        if self.__con is not None:
            cur = self.__con.cursor()
            cur.execute("REPLACE INTO " + self.table_name + " (rule1, rule2, value) VALUES (?,?,?)", (rule1, rule2, value))
            self.__con.commit()
            lid = self.__con.total_changes
            self.lastrowid = cur.lastrowid
        return lid

    def clear_table(self):
        """ Удаляет все записи из таблицы "tableName"
        :return:
        """
        if self.__con is not None:
            try:
                cur = self.__con.cursor()
                cur.execute("DELETE FROM " + self.table_name)
                self.__con.commit()
            except sqlite3.DatabaseError as err:
                return err
            else:
                return
            finally:
                return

    def get_rec(self, key1, key2):
        if self.__con is not None:
            cur = self.__con.cursor()
            sql = "select rule1, rule2, value FROM " + self.table_name + " where rule1=? and rule2=?"
            cur.execute(sql, (key1, key2))
            row = cur.fetchall()
            return row

    def get_all_recs(self):
        if self.__con is not None:
            cur = self.__con.cursor()
            sql = "select rule1, rule2, value FROM " + self.table_name
            cur.execute(sql)
            row = cur.fetchall()
            return row

    def count_recs(self):
        if self.__con is not None:
            cur = self.__con.cursor()
            cur.execute("select count(*) as countRecs FROM " + self.table_name)
            row = cur.fetchone()
            return row[0]
        return 0

    def get_recs(self, start, count):
        self.__start = 1
        self.__count = -1
        self.curPos = -1
        if self.__con is not None:
            self.__start = start
            self.__count = count
            self.curPos = start+count
            self.__cursor = self.__con.cursor()
            sql = "select rule1, rule2, value FROM " + self.table_name + " LIMIT ?  OFFSET ?"
            self.__cursor.execute(sql, (self.__count, self.__start))
            row = self.__cursor.fetchall()
            return row

    def __next__(self):
        """
        """
        if self.__con is not None:
            if self.__cursor is not None:
                sql = "select rule1, rule2, value FROM " + self.table_name + " LIMIT ? OFFSET ?"
                self.__cursor.execute(sql, (self.__count, self.curPos))
                row = self.__cursor.fetchall()
                if len(row) == 0:
                    raise StopIteration()
                self.curPos += self.__count
                return row
        return StopIteration()

    def __iter__(self):
        return self


