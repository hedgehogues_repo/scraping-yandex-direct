import unittest

from selenium.webdriver import Chrome

from internal.driver import Driver
from internal.captchaProcessor import CaptchaProcessor
from internal.io import ReadWriter, ReadWriterSQLite
from internal.scraper import Scraper


class TestScraper(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.pwd = '50shadofpars'
        cls.login = 'art.parser'
        cls.driver_path = '../../libs/chromedriver'
        cls.intersections = [
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны'],
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'КЛИЕНТ / Артус\nсегмент Метрики'],
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'ВК / Бородатые (п1)\nпохожий сегмент'],
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', '11\nгео регулярные'],
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'B14\nгео регулярные'],
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'D03\nгео регулярные'],
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'H17\nгео регулярные'],
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'I16\nгео регулярные'],
            ['БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'J10\nгео регулярные'],
            ['ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны', 'КЛИЕНТ / Артус\nсегмент Метрики'],
            ['ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны', 'ВК / Бородатые (п1)\nпохожий сегмент'],
            ['ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны', '11\nгео регулярные'],
            ['ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны', 'B14\nгео регулярные'],
            ['ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны', 'D03\nгео регулярные'],
            ['ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны', 'H17\nгео регулярные'],
            ['ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны', 'I16\nгео регулярные'],
            ['ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны', 'J10\nгео регулярные'],
            ['КЛИЕНТ / Артус\nсегмент Метрики', 'ВК / Бородатые (п1)\nпохожий сегмент'],
            ['КЛИЕНТ / Артус\nсегмент Метрики', '11\nгео регулярные'],
            ['КЛИЕНТ / Артус\nсегмент Метрики', 'B14\nгео регулярные'],
            ['КЛИЕНТ / Артус\nсегмент Метрики', 'D03\nгео регулярные'],
            ['КЛИЕНТ / Артус\nсегмент Метрики', 'H17\nгео регулярные'],
            ['КЛИЕНТ / Артус\nсегмент Метрики', 'I16\nгео регулярные'],
            ['КЛИЕНТ / Артус\nсегмент Метрики', 'J10\nгео регулярные'],
            ['ВК / Бородатые (п1)\nпохожий сегмент', '11\nгео регулярные'],
            ['ВК / Бородатые (п1)\nпохожий сегмент', 'B14\nгео регулярные'],
            ['ВК / Бородатые (п1)\nпохожий сегмент', 'D03\nгео регулярные'],
            ['ВК / Бородатые (п1)\nпохожий сегмент', 'H17\nгео регулярные'],
            ['ВК / Бородатые (п1)\nпохожий сегмент', 'I16\nгео регулярные'],
            ['ВК / Бородатые (п1)\nпохожий сегмент', 'J10\nгео регулярные'],
            ['11\nгео регулярные', 'B14\nгео регулярные'],
            ['11\nгео регулярные', 'D03\nгео регулярные'],
            ['11\nгео регулярные', 'H17\nгео регулярные'],
            ['11\nгео регулярные', 'I16\nгео регулярные'],
            ['11\nгео регулярные', 'J10\nгео регулярные'],
            ['B14\nгео регулярные', 'D03\nгео регулярные'],
            ['B14\nгео регулярные', 'H17\nгео регулярные'],
            ['B14\nгео регулярные', 'I16\nгео регулярные'],
            ['B14\nгео регулярные', 'J10\nгео регулярные'],
            ['D03\nгео регулярные', 'H17\nгео регулярные'],
            ['D03\nгео регулярные', 'I16\nгео регулярные'],
            ['D03\nгео регулярные', 'J10\nгео регулярные'],
            ['H17\nгео регулярные', 'I16\nгео регулярные'],
            ['H17\nгео регулярные', 'J10\nгео регулярные'],
            ['I16\nгео регулярные', 'J10\nгео регулярные'],
        ]
        cls.names = [
            'БИЗНЕСМЕНЫ / Начинающие\nэл. почта', 'ВЛАДЕЛЬЦЫ / Прямые номера\nтелефоны',
            'КЛИЕНТ / Артус\nсегмент Метрики', 'ВК / Бородатые (п1)\nпохожий сегмент', '11\nгео регулярные',
            'B14\nгео регулярные', 'D03\nгео регулярные', 'H17\nгео регулярные', 'I16\nгео регулярные',
            'J10\nгео регулярные'
        ]

    def scraper_create(self, readwriter):
        base_driver = Chrome(executable_path=self.driver_path)
        custom_driver = Driver(
            base_driver=base_driver,
            captcha=CaptchaProcessor(base_driver=base_driver, captcha_repeat_times=None, captcha_timeout=5000),
            action_timeout=300, random_timeout=100, enter_text_timeout=10,
            random_big_timeout=10000, repeat_big_timeout_tics=100)
        return Scraper(custom_driver=custom_driver, readwriter=readwriter, login=self.login, pwd=self.pwd)

    def implementation(self, readwriter):
        scraper = self.scraper_create(readwriter=readwriter)

        # Test audiences
        scraper.login()
        audiences = scraper.get_audience_names()
        for aud, target in zip(audiences, self.names):
            self.assertEqual(target, aud)

        # Flush state of browser
        scraper.flush()

        # Test Scrapy
        scraper.login()
        scraper.scrapy()
        answ_ind = 0
        for ind, aud0 in enumerate(audiences[:-1]):
            for aud1 in audiences[ind + 1:]:
                item = scraper.read_intersection(aud0, aud1)
                self.assertEqual(item[0], self.intersections[answ_ind][0])
                self.assertEqual(item[1], self.intersections[answ_ind][1])
                self.assertEqual(int, type(item[2]))
                answ_ind += 1

        # Exit
        scraper.close()

    def test_memory(self):
        self.implementation(ReadWriter())

    def test_memory_partial_audiences(self):
        scraper = self.scraper_create(ReadWriter())
        scraper.scrapy(audiences='КЛИЕНТ / Артус\nсегмент Метрики')
        scraper.close()

    def test_DB(self):
        self.implementation(ReadWriterSQLite('../../data/test/scrapy/test.db'))
