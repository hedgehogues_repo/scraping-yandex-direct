import unittest
from internal.sqliteDb import SqliteDb
from internal.io import ReadWriterSQLite


class ioTests(unittest.TestCase):
    """ReadWriterSQLite tests"""
    fileName = "../../data/test/DB/testCampaigns.db"

    def setUp(self):
        """Set up for test"""
        db = SqliteDb(self.fileName)
        db.connect()
        db.clear_table()
        db.update_rec_campaigns("rule11", "rule12", 1001)
        db.update_rec_campaigns("rule21", "rule22", 1002)
        db.update_rec_campaigns("rule31", "rule32", 1003)
        db.update_rec_campaigns("rule41", "rule42", 1004)
        db.update_rec_campaigns("rule51", "rule52", 1005)
        db.update_rec_campaigns("rule61", "rule62", 1006)
        db.close()

    def test_getFirstRec(self):
        """test_addRec test"""
        db_reader = ReadWriterSQLite(self.fileName)
        campaign = db_reader.read("rule11", "rule12")
        self.assertTupleEqual(campaign[0], ("rule11", "rule12", 1001))

    def test_getMiddleRec(self):
        """test_addRec test"""
        db_reader = ReadWriterSQLite(self.fileName)
        campaign = db_reader.read("rule31", "rule32")
        self.assertTupleEqual(campaign[0], ("rule31", "rule32", 1003))

    def test_getLastRec(self):
        """test_addRec test"""
        db_reader = ReadWriterSQLite(self.fileName)
        campaign = db_reader.read("rule61", "rule62")
        self.assertTupleEqual(campaign[0], ("rule61", "rule62", 1006))

    def test_getEmptyRec(self):
        """getEmptyRec test"""
        db_reader = ReadWriterSQLite(self.fileName)
        campaign = db_reader.read("rule33", "rule33")
        self.assertEqual(len(campaign), 0)

    def test_writeRec(self):
        """test_addRec test"""
        db_reader = ReadWriterSQLite(self.fileName)
        db_reader.write("rule71", "rule72", 1007)
        campaign = db_reader.read("rule71", "rule72")
        self.assertTupleEqual(campaign[0], ("rule71", "rule72", 1007))

    def test_updateRec(self):
        """test_addRec test"""
        db_reader = ReadWriterSQLite(self.fileName)
        db_reader.write("rule31", "rule32", 1033)
        campaign = db_reader.read("rule31", "rule32")
        self.assertTupleEqual(campaign[0], ("rule31", "rule32", 1033))


if __name__ == '__main__' :
    unittest.main()
