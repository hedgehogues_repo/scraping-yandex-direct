import unittest
from internal.sqliteDb import SqliteDb


class SqliteDbTests(unittest.TestCase):
    """SqliteDb tests"""
    path = "../../data/test/DB/"
    fileName = path + "testCampaigns.db"

    @classmethod
    def setUpClass(cls):
        """Set up for class"""
        pass

    @classmethod
    def tearDownClass(cls):
        """Tear down for class"""
        pass

    def setUp(self):
        """Set up for test"""
        pass

    def tearDown(self):
        """Tear down for test"""
        pass

    def test_connect(self):
        """test_connect test"""
        file_name_tmp = self.path + "test.db"
        db = SqliteDb(file_name_tmp)
        self.assertEqual(db.connect(), True)

    def test_close(self):
        """test_close test"""
        file_name_tmp = self.path + "myDb.db"
        db = SqliteDb(file_name_tmp)
        self.assertEqual(db.close(), True)

    def test_addRec(self):
        """test_close test"""
        db = SqliteDb(self.fileName, "campaigns")
        db.connect()
        db.clear_table()
        db.add_rec_campaigns("rule1", "rule2", 1000)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 1)

    def test_getRec(self):
        """test_getRec test"""
        db = SqliteDb(self.fileName, "campaigns")
        db.connect()
        db.clear_table()
        db.add_rec_campaigns("rule11", "rule21", 1001)
        db.add_rec_campaigns("rule12", "rule22", 1002)
        db.add_rec_campaigns("rule13", "rule23", 1003)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 3 )
        rec = db.get_rec("rule12", "rule22")
        self.assertEqual(len(rec), 1 )
        self. assertTupleEqual(rec[0], ("rule12", "rule22", 1002))

    def test_getAllRecs(self):
        """test_getRec test"""
        db = SqliteDb(self.fileName, "campaigns")
        db.connect()
        db.clear_table()
        db.add_rec_campaigns("rule11", "rule21", 1001)
        db.add_rec_campaigns("rule12", "rule22", 1002)
        db.add_rec_campaigns("rule13", "rule23", 1003)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 3)
        rec = db.get_all_recs()
        self.assertEqual(len(rec), 3)
        self.assertTupleEqual(rec[0], ("rule11", "rule21", 1001))
        self.assertTupleEqual(rec[1], ("rule12", "rule22", 1002))
        self.assertTupleEqual(rec[2], ("rule13", "rule23", 1003))

    def test_count_recs(self):
        """test_close test"""
        # print(" [" + self.shortDescription() + "]")
        db = SqliteDb(self.fileName)
        db.connect()
        db.clear_table()
        db.add_rec_campaigns("rule11", "rule21", 1000)
        db.add_rec_campaigns("rule12", "rule22", 1000)
        db.add_rec_campaigns("rule13", "rule23", 1000)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 3 )

    def test_clearTable(self):
        """test_clearTable test"""
        db = SqliteDb(self.fileName)
        db.connect()
        db.add_rec_campaigns("rule11", "rule21", 1000)
        db.add_rec_campaigns("rule12", "rule22", 1000)
        db.add_rec_campaigns("rule13", "rule23", 1000)
        db.clear_table()
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 0 )

    def test_clearUnexistedTable(self):
        """test_clearTable test"""
        db = SqliteDb(self.fileName)
        db.connect()
        db.table_name = "error_table"
        err = db.clear_table()
        self.assertFalse(err)


if __name__ == '__main__' :
    unittest.main()
