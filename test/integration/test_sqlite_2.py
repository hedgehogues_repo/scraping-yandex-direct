import sqlite3
import unittest
from internal.sqliteDb import SqliteDb


class SqliteDbTests(unittest.TestCase):
    """SqliteDb tests"""
    fileName = "../../data/test/DB/testCampaigns.db"

    def test_addDuplicateRecs(self):
        """test_close test"""
        db = SqliteDb(self.fileName)
        db.connect()
        db.clear_table()
        db.add_rec_campaigns("rule1", "rule2", 1000)

        with self.assertRaises(sqlite3.IntegrityError) : db.add_rec_campaigns("rule1", "rule2", 1000)

    def test_addRec(self):
        """test_addRec test"""
        db = SqliteDb(self.fileName, "campaigns")
        db.connect()
        db.clear_table()
        db.update_rec_campaigns("rule1", "rule2", 1000)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 1)
        rec = db.get_rec("rule1", "rule2")
        self.assertEqual(len(rec), 1)
        self.assertTupleEqual(rec[0], ("rule1", "rule2", 1000))

    def test_updateRec(self):
        """ test_addRec test """
        db = SqliteDb(self.fileName, "campaigns")
        db.connect()
        db.clear_table()
        db.update_rec_campaigns("rule1", "rule2", 1000)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 1)
        rec = db.get_rec("rule1", "rule2")
        self.assertEqual(len(rec), 1)
        self.assertTupleEqual(rec[0], ("rule1", "rule2", 1000))
        db.update_rec_campaigns("rule1", "rule2", 1001)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 1)
        rec = db.get_rec("rule1", "rule2")
        self.assertEqual(len(rec), 1)
        self.assertTupleEqual(rec[0], ("rule1", "rule2", 1001))

    def test_updateRec2(self):
        """test_addRec test"""
        db = SqliteDb(self.fileName)
        db.connect()
        db.clear_table()
        db.update_rec_campaigns("rule11", "rule12", 1001)
        db.update_rec_campaigns("rule21", "rule22", 1002)
        db.update_rec_campaigns("rule31", "rule32", 1003)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 3)
        rec = db.get_rec("rule21", "rule22")
        self.assertEqual(len(rec), 1)
        self.assertTupleEqual(rec[0], ("rule21", "rule22", 1002))
        db.update_rec_campaigns("rule21", "rule22", 1012)
        self.assertEqual(count_recs, 3)
        rec = db.get_rec("rule21", "rule22")
        self.assertEqual(len(rec), 1)
        self.assertTupleEqual(rec[0], ("rule21", "rule22", 1012))

    def test_getRecs(self):
        """test_addRec test"""
        db = SqliteDb(self.fileName)
        db.connect()
        db.clear_table()
        db.update_rec_campaigns("rule11", "rule12", 1001)
        db.update_rec_campaigns("rule21", "rule22", 1002)
        db.update_rec_campaigns("rule31", "rule32", 1003)
        db.update_rec_campaigns("rule41", "rule42", 1004)
        db.update_rec_campaigns("rule51", "rule52", 1005)
        db.update_rec_campaigns("rule61", "rule62", 1006)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 6)
        rec = db.get_recs(0, 1)
        self.assertEqual(len(rec), 1)
        self.assertTupleEqual(rec[0], ("rule11", "rule12", 1001))

    def test_getRecs2(self):
        """test_addRec test"""
        db = SqliteDb(self.fileName)
        db.connect()
        db.clear_table()
        db.update_rec_campaigns("rule11", "rule12", 1001)
        db.update_rec_campaigns("rule21", "rule22", 1002)
        db.update_rec_campaigns("rule31", "rule32", 1003)
        db.update_rec_campaigns("rule41", "rule42", 1004)
        db.update_rec_campaigns("rule51", "rule52", 1005)
        db.update_rec_campaigns("rule61", "rule62", 1006)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 6)
        rec = db.get_recs(0, 3)
        self.assertEqual(len(rec), 3)
        self.assertTupleEqual(rec[0], ("rule11", "rule12", 1001))
        self.assertTupleEqual(rec[1], ("rule21", "rule22", 1002))
        self.assertTupleEqual(rec[2], ("rule31", "rule32", 1003))

    def test_iterator(self):
        """test_addRec test"""
        db = SqliteDb(self.fileName)
        db.connect()
        db.clear_table()
        db.update_rec_campaigns("rule11", "rule12", 1001)
        db.update_rec_campaigns("rule21", "rule22", 1002)
        db.update_rec_campaigns("rule31", "rule32", 1003)
        db.update_rec_campaigns("rule41", "rule42", 1004)
        db.update_rec_campaigns("rule51", "rule52", 1005)
        db.update_rec_campaigns("rule61", "rule62", 1006)
        count_recs = db.count_recs()
        self.assertEqual(count_recs, 6)
        rec = db.get_recs(0, 3)
        self.assertEqual(len(rec), 3)
        self.assertTupleEqual(rec[0], ("rule11", "rule12", 1001))
        self.assertTupleEqual(rec[1], ("rule21", "rule22", 1002))
        self.assertTupleEqual(rec[2], ("rule31", "rule32", 1003))
        # iRec = iter(db)
        # rec=next(iRec)
        rec = next(db)
        self.assertEqual(len(rec), 3)
        self.assertTupleEqual(rec[0], ("rule41", "rule42", 1004))
        self.assertTupleEqual(rec[1], ("rule51", "rule52", 1005))
        self.assertTupleEqual(rec[2], ("rule61", "rule62", 1006))
        with self.assertRaises(StopIteration):
            rec = next(db)
        # self.assertEqual(len(rec), 3)


if __name__ == '__main__' :
    unittest.main()
